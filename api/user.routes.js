const express = require('express');
const router = express.Router();

router.get('/user', (req, res) => {
    res.status(200).json({
        userName: "Niels",
        dateOfBirth: "04-04-1994",
        age: "26"
    });
});

module.exports = router;